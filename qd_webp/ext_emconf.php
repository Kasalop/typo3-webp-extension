

<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "qd_webp"
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => '[Qualidev] Webp Media Manager',
    'description' => '',
    'category' => 'be', // oder be?
//  'shy' => 0, // ??
    'version' => '0.0.2',
//  'module' => '', // ??
    'state' => 'alpha', // oder beta / stable
    'uploadfolder' => false,
//  'createDirs' => '', // ??
    'clearCacheOnLoad' => 1, // alternativ 0
//  'locktype' => '', // ??
    'author' => 'Joshua Kuehner',
    'author_email' => 'typo3-extensions@mail-an-jk.de',
    'author_company' => 'Qualidev',
    'constraints' => [
        'depends' => [
            'php' => '7.0.0-7.3.0',
            'typo3' => '6.1.0-7.9.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
