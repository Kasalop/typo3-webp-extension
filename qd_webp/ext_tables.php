<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

\Typo3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin('Qualidev.' . $_EXTKEY, 'QualidevWebp', 'Plugin that implements a backend module for uploading images in webp format and a frontend module for displaying these webp images.');

if(TYPO3_MODE === "BE") {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'Qualidev.' . $_EXTKEY,     // vendor + extkey, seperated by a dot
        'user',                     // Backend Module group to place the module in
        'webpmanagement',           // module name
        '',                         // position in the group
        array(                      // Allowed controller -> action combinations
            'Biblio' => 'list, add, remove',
            'Image' => 'show, new, delete'
        ),
        array(                      // Additional configuration
            'access' => 'user,group',
            'icon' => 'EXT:qd_webp/ext_icon.png',
            'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_mod.xlf',
        )
    );
}