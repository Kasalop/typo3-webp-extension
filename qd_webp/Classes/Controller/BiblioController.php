<?php
namespace Qualidev\QdWebp\Controller;

use Qualidev\QdWebp\ImageOrganizer;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

define('DEACTIVATED', false);

/**
 * The main Controller, managing all the tasks for biblio management
 */
class BiblioController extends ActionController {

    /**
     * initial action, called on a clean request without specified target
     */
    public function listAction() {
        $resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();
        $defaultStorage = $resourceFactory->getDefaultStorage();
        $folder = $defaultStorage->getFolder('/files');
        $files = $defaultStorage->getFilesInFolder($folder);

        // Informations for further work. Not related to this method anymore.

        // $file->process() zum konvertieren und zuschneiden?
        // $file->getIdentifier() oder $file->getOriginalFile()->getIdentifier()
        // getModificationTime()
        // getName()
        // getNameWithoutExtension()
        // getSha1()
        // getSize()
        // getMimeType()
        // getType()
        // getCreationTime()
        // getExtension()
        // getModificationTime()
        // needsReprocessing()

        // ImageOrganizer takes pictures and groups them by there name (without extension)
        $imageOrganizer = new ImageOrganizer();
        foreach($files as $fileName => $file) {
            // if not an image: skip this file
            if($file->getType() !== \TYPO3\CMS\Core\Resource\AbstractFile::FILETYPE_IMAGE) continue;
            // otherwise:
            $imageOrganizer->addImage($file);
        }

        $this->view->assign('groupOfImages', $imageOrganizer->getGroups());
        $this->view->assign('maxUploadFileSize', \TYPO3\CMS\Core\Utility\GeneralUtility::getMaxUploadFileSize()*1024); // getMaxUploadFileSize() returns number in KB, so multiply with 1024 to get the number of bytes. Now we can use the format.bytes viewhelper

        // Add some usefull informations
        $imageManipulator = new \Qualidev\QdWebp\Service\ImageManipulation();
        $this->view->assign('systemAccessEnabled', $imageManipulator->checkSystemAccess());
        $this->view->assign('webpSupport', $imageManipulator->checkWebpInstallation());
        $this->view->assign('filesystemAccess', $imageManipulator->checkFilesystemAccess());
        $this->view->assign('convertSuccess', $imageManipulator->checkConvertToWebp());
    }

    /**
     * add action, called on form request to add images
     * redirects to the initial (list) action
     */
    public function addAction() {
        if (isset($_FILES) && !empty($_FILES)) {
            $resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();
            $defaultStorage = $resourceFactory->getDefaultStorage();
            $folder = $defaultStorage->getFolder('/files');

            foreach ($_FILES['images']['name'] as $key => $filename) {
                if (!empty($filename)) {
                    if(explode("/", $_FILES['images']['type'][$key])[0]!=='image') continue;

                    $resource = [
                        'tmp_name' => $_FILES['images']['tmp_name'][$key],
                        'name'     => $_FILES['images']['name'][$key]
                    ];

                    $folder->addUploadedFile($resource, 'replace' /* Modern way, but not supported in 6.2: \TYPO3\CMS\Core\Resource\DuplicationBehavior::REPLACE */);
                }
            }
        }
        $this->forward('list');
    }

    public function removeAction() {
        throw new \Qualidev\QdWebp\NotImplementedException("remove action is not defined yet.");
    }

    /**
     * @return void
     */
    public function initializeAction() {

        // deactivated at the moment. Left for further work.
        if(TYPO3_MODE === 'BE' && DEACTIVATED) {
            // old style PHP / T3: 'Tx_Extbase_Configuration_BackendConfigurationManager'
            $configurationManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Configuration\BackendConfigurationManager::class);
            $this->settings = $configurationManager->getConfiguration(
                \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK
                //\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS
                //$this->request->getControllerExtensionName(),
                //$this->request->getPluginName()
            );
        }

    }
}