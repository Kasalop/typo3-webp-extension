<?php

namespace Qualidev\QdWebp\ViewHelpers;

class DimensionViewHelper extends \TYPO3\CMS\Fluid\Core\Widget\AbstractWidgetViewHelper {

    /**
     * @var \Qualidev\QdWebp\ViewHelpers\Controller\DimensionController
     * @inject
     */
    protected $controller;

    /**
     * The render method of widget
     *
     * @param int $width
     * @param int $height
     * @return string
     */
    public function render($width=0, $height=0) {
        return $width."px / ".$height."px";
    }
}