<?php
/**
 * Created by PhpStorm.
 * User: Joshua Kühner
 * Date: 26.08.2018
 * Time: 14:00
 */

namespace Qualidev\QdWebp\ViewHelpers\Hash;


class Sha1ViewHelper extends \TYPO3\CMS\Fluid\Core\Widget\AbstractWidgetViewHelper {

    /**
     * @var \Qualidev\QdWebp\ViewHelpers\Controller\DummyController
     * @inject
     */
    protected $controller;

    /**
     * The render method of widget
     *
     * @param string $hash
     * @param bool $convert
     * @param bool $wrap
     * @return string
     */
    public function render($hash=NULL, $convert=false, $wrap=true) {
        $hash = $hash ?? $this->renderChildren();
        $convert = is_bool($convert) ? $convert : strtolower($convert)==='true';
        $wrap = is_bool($wrap) ? $wrap : strtolower($wrap)==='true';

        if($convert)
            $hash = sha1($hash);
        $html = sprintf('<span class="qd-widget-hash qd-widget-hash-sha1">%s</span>', $hash);
        return $wrap ? $html : $hash;
    }
}