<?php
/**
 * Created by PhpStorm.
 * User: Joshua Kühner
 * Date: 26.08.2018
 * Time: 14:00
 */

namespace Qualidev\QdWebp\ViewHelpers\Hash;


class Md5ViewHelper extends \TYPO3\CMS\Fluid\Core\Widget\AbstractWidgetViewHelper {

    /**
     * @var \Qualidev\QdWebp\ViewHelpers\Controller\DummyController
     * @inject
     */
    protected $controller;

    /**
     * The render method of widget
     *
     * @param string $hash
     * @return string
     */
    public function render($hash=NULL) {
        if($hash === NULL) {
            $hash = $this->renderChildren();
        }
        return '<span class="qd-widget-hash qd-widget-hash-md5">'.$hash.'</span>';
    }
}