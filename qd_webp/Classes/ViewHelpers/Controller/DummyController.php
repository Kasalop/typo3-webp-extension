<?php
/**
 * Created by PhpStorm.
 * User: Joshua Kühner
 * Date: 26.08.2018
 * Time: 03:01
 */

namespace Qualidev\QdWebp\ViewHelpers\Controller;


class DummyController extends \TYPO3\CMS\Fluid\Core\Widget\AbstractWidgetController {
    /**
     * Initialize Action of the widget controller
     *
     * @return void
     */
    public function initializeAction() {}
}