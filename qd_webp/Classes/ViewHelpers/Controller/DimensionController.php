<?php
/**
 * Created by PhpStorm.
 * User: Joshua Kühner
 * Date: 26.08.2018
 * Time: 03:01
 */

namespace Qualidev\QdWebp\ViewHelpers\Controller;


class DimensionController extends \TYPO3\CMS\Fluid\Core\Widget\AbstractWidgetController {
    /**
     * Initialize Action of the widget controller
     *
     * @return void
     */
    public function initializeAction() {
        $this->configuration = \TYPO3\CMS\Core\Utility\GeneralUtility::array_merge_recursive_overrule($this->configuration, $this->widgetConfiguration['configuration'], TRUE);
    }
}