<?php
/**
 * Created by PhpStorm.
 * User: Joshua Kühner
 * Date: 26.08.2018
 * Time: 20:24
 */

namespace Qualidev\QdWebp\ViewHelpers;


class PictureViewHelper extends \TYPO3\CMS\Fluid\Core\Widget\AbstractWidgetViewHelper {

    /**
     * @var \Qualidev\QdWebp\ViewHelpers\Controller\DummyController
     * @inject
     */
    protected $controller;

    /**
     * The render method of widget
     *
     * @param string $hash
     * @return string
     */
    public function render() {
        // Ausgabe in Abhängigkeit vom Server feld Accept (Opera mini)
        // Ansonsten Picture-Element mit js fallback
        // IE kann kein Picture, aber auch kein webp

    }
}