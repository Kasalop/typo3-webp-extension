<?php
/**
 * Created by PhpStorm.
 * User: Joshua Kühner
 * Date: 27.08.2018
 * Time: 01:13
 */

namespace Qualidev\QdWebp\Service;


class ImageManipulation {
    public function checkSystemAccess() {
        return function_exists('shell_exec'); // true | false
    }
    public function checkWebpInstallation() {
        $result = `dpkg -s webp | grep Status`;
        return strpos($result, 'install ok installed') !== false; // true | false
    }
    public function checkFilesystemAccess() {
        $resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();
        $defaultStorage = $resourceFactory->getDefaultStorage();
        $folder = $defaultStorage->getFolder('/files');

        return is_writable(\TYPO3\CMS\Core\Utility\PathUtility::getCanonicalPath(PATH_site.$folder->getPublicUrl())); // true | false
    }
    public function checkConvertToWebp() {
        $resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();
        $defaultStorage = $resourceFactory->getDefaultStorage();
        $folder = $defaultStorage->getFolder('/files');

        $testImagePath = \TYPO3\CMS\Core\Utility\PathUtility::getCanonicalPath(PATH_site.$folder->getPublicUrl()).'/ext_demo_png.png';
        $convertedImagePath = \TYPO3\CMS\Core\Utility\PathUtility::getCanonicalPath(PATH_site.$folder->getPublicUrl()).'/ext_demo_png.webp';
        $im = imagecreatetruecolor(1,1);
        $result = imagepng($im, $testImagePath);
        imagedestroy($im);

        `convert {$testImagePath} -quality 1 -define webp:lossless=true {$convertedImagePath}`;

        $result = file_exists($convertedImagePath);

        unlink($testImagePath);
        unlink($convertedImagePath);
        return $result;
    }
}