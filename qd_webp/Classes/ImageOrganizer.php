<?php
/**
 * Created by PhpStorm.
 * User: Joshua Kühner
 * Date: 25.08.2018
 * Time: 22:26
 */

namespace Qualidev\QdWebp;

use http\Exception\InvalidArgumentException;
use Qualidev\QdWebp\NotImplementedException;

class ImageOrganizer {
    protected $_groupOfImages = [];

    public function addImage(\TYPO3\CMS\Core\Resource\AbstractFile $image) {
        if($image->getType() !== \TYPO3\CMS\Core\Resource\AbstractFile::FILETYPE_IMAGE) {
            throw new InvalidArgumentException("This function should only be called with an image as argument.");
        }

        $filenameWithoutExtension = $image->getNameWithoutExtension();
        if(!array_key_exists($filenameWithoutExtension, $this->_groupOfImages)) {
            $this->_groupOfImages[$filenameWithoutExtension] = [
                "name" => $filenameWithoutExtension,
                "images" => []
            ];
        }
        $absolutePath = \TYPO3\CMS\Core\Utility\PathUtility::getCanonicalPath(PATH_site.$image->getPublicUrl());
        $imagesize = $this->getImageDimensions($absolutePath);

        $this->_groupOfImages[$filenameWithoutExtension]['images'][] = [
            "name" => $image->getName(),
            "extension" => $image->getExtension(),
            "sha1" => $image->getSha1(),
            "identifier" => $image->getIdentifier(),
            "mime" => $image->getMimeType(),
            "sizeInBytes" => $image->getSize(),
            "dimensions" => [
                "width" => $imagesize[0],
                "height" => $imagesize[1]
            ],
            "filetime_creation" => $image->getCreationTime(),
            "filetime_modification" => $image->getModificationTime(),
            "uri" => $absolutePath
        ];
    }
    protected function getImageDimensions($filename) {
        // getimagesize supports images of type "webp" since version PHP v7.1.0
        // before 7.1.0 we have to use IM or gd with support for webp format
        if(version_compare(PHP_VERSION, '7.1.0', '<'))
            return call_user_func(function($filename) {
                // -- PHP 7.1.0 is from 12.2016 => No fallback provided by me. If needed it can be implemented here
                // ... code that gets the resolution from webp images ...

                // if fallback is implemented, please comment the following line
                throw new NotImplementedException("getimagesize for webp works since PHP v7.1.0. You're using ".PHP_VERSION.". Please update your PHP version or provide a fallback in ".__FILE__." on line ".__LINE__.".");
            }, $filename);
        else
            return getimagesize($filename);
    }

    public function getGroups() {
        return $this->_groupOfImages;
    }
}